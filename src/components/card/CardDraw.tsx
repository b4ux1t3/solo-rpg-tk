import { ChangeEvent, useState } from "react";
import { CardView, CardViewProps } from "./Card";
import { generateDeck } from "../../models/card";

import './CardDraw.css';

const initialHand: CardViewProps[] = []

export const CardDraw = () => {
    const [hand, setHand] = useState(initialHand);
    const [numDraw, setnumDraw] = useState(1);
    const [addJokers, setAddJokers] = useState(true);
    const [deck, setDeck] = useState(genDeck(true));
    const shuffle = (): CardViewProps[] => {
        const newDeck = genDeck(addJokers);
        setDeck([...newDeck]);
        setHand([]);
        setnumDraw(1);  
        return newDeck;
    }
    const draw = (): void =>{
        hand.reverse();
        const newHand = [...hand];
        for (let i = 0; i < numDraw; i++){

            const newCard = deck.pop();
            if (newCard === undefined) {
                shuffle();
                
                return;
            }
            newHand.push(newCard);
        }
        newHand.reverse();
        setHand([...newHand]);
        setDeck([...deck]);
    }
    const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const num = Math.min(Number.parseInt(event.target.value), deck.length);
        setnumDraw(num);
    }

    const drawButton = () => 
        (<>
            <input 
                type="number" 
                name="num-to-draw" 
                id="num-to-draw" 
                value={numDraw}
                onChange={onInputChange}
                max={deck.length}
                min={1} />
            <button onClick={draw} disabled={deck.length < 1}>Draw</button>
        
        </>);
    const shuffleButton = () => 
        (<button onClick={shuffle} disabled = {hand.length < 1}>Shuffle</button>);
    return (
      <div className="grid card-grid">      
        <div className="flex row gap-2 align-center justify-center bottom-white-divider">
            {drawButton()}
            {shuffleButton()}
            <div>
                <label htmlFor="add-jokers">Jokers?</label>
                <input 
                    type="checkbox" 
                    name="add-jokers" 
                    id="add-jokers" 
                    checked={addJokers} 
                    onInput={() => setAddJokers(!addJokers)}/>
            </div>
        </div>  
        <div className="card hand default-border">
          {
            hand.map(c => CardView(c))
          }
        </div>
      </div>);
}

const genDeck = (addJokers: boolean) =>
    shuffle(generateDeck(addJokers)
        .map(c =>
            ({ Card: c, Id: crypto.randomUUID() })
        )
    );

function shuffle<T>(array: T[]): T[] {
    const arr = [...array]
    let currentIndex = arr.length;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {

        // Pick a remaining element...
        const randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [arr[currentIndex], arr[randomIndex]] = [
            arr[randomIndex], arr[currentIndex]];
    }

    return arr;
}