import { Card } from "../../models/card";
import './Card.css';
export interface CardViewProps {
    Card: Card;
    Id: string;
}
const getSymbol = (c: Card) => 
    {
        switch (c.Suit){
            case 'Hearts':  
                return '♥️';
            case 'Spades':
                return '♠️'  ;
            case 'Diamonds': 
                return '♦️' ;
            case 'Clubs': 
                return '♣️' ; 
        }
}

export const CardView = (props: CardViewProps) => {


    return (
    <div className="card-display">
        <div className="float-left">{getSymbol(props.Card)}</div>
        <div className="float-center">{props.Card.Rank}</div>
        <div className="float-right">{getSymbol(props.Card)}</div>
    </div>);
}