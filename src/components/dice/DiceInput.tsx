import { ChangeEvent, FormEvent, useState } from "react";
import { rand } from "../../models/dice";

const diceRegex = /(\d+)?d(\d+)\s*(([-+/*])\s*(\d+))?/;

export const DiceInput = () => {
    const [input, setInput] = useState('');
    const [output, setOutput] = useState(0);
    const [inputOkay, setInputOkay] = useState(false);
    const onInputChanged = (event: ChangeEvent<HTMLInputElement>): void => {
        setInput(event.target.value);
        setInputOkay(diceRegex.test(event.target.value));
    }

    const roll = (event: FormEvent) => {
        event.preventDefault();

        const matches = diceRegex.exec(input)
        if (matches){
            const numberOfDice = Number.parseInt(matches[1]);
            const dieValue = Number.parseInt(matches[2]);
            const operation = matches[4];
            const modifier = Number.parseInt(matches[5])

            let result = 0;
            if (numberOfDice && dieValue){
                
                for (let i = 0; i < numberOfDice; i++){
                    result += rand(dieValue);
                }
            } else if (dieValue){
                result += rand(dieValue);
            }

            if(result > 0){
                if (operation && modifier > 0){
                    switch (operation) {
                        case '/':
                            result/= modifier;
                            break;
                        case '*':
                            result*= modifier;
                            break;
                        case '+':
                            result+= modifier;
                            break;
                        case '-':
                            result-= modifier;
                            break;
                        default:
                            break;
                    }
                }
                setOutput(result);
            }

        }
    }

    return (<>
    <form onSubmit={roll} className="flex row gap-2 justify-center">
        <div><p className="example-text">e.g. 2d6+2, d20+3, 2d4*2</p></div>
        <input
            value={input}
            onChange={onInputChanged}
            className={inputOkay ? '' : 'invalid-input'}
            />
        <button type='submit' disabled={!inputOkay}>Roll</button>
        <button onClick={()=>setInput('')}>🗑️</button>
    </form>
        <p>{output > 0 ? `Result: ${output}` : ''}</p>
    </>);
};
