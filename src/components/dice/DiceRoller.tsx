import { supportedDice } from "../../models/dice";
import { DiceButton } from "./DiceButton";

export const DiceRoller = () => {
    return (
        <>
            <div className="flex row wrap gap-1 align-content-start justify-content-center">
                {supportedDice.map(d => <DiceButton n={d} key={crypto.randomUUID()}/>)}
            </div>
        </>
    )
}