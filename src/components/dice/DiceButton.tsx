import { useState } from "react";
import './DiceButton.css';
export interface diceProps {
    n: number,
    key: string
}
export const DiceButton = (props: diceProps) => {
    const rand = (n: number): number => Math.ceil(Math.random() * n);
    const [lastRoll, setLastRoll] = useState(0);
    return (<div>
        <p>d{props.n}</p>
        <input type="button" className='dice-button' value="🎲" onClick={() => setLastRoll(rand(props.n))} />
        <p>{lastRoll === 0 ? '' : `${lastRoll}`}</p>
    </div>);
};
