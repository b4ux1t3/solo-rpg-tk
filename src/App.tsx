import './App.css';
import { CardDraw } from './components/card/CardDraw';
import { DiceInput } from './components/dice/DiceInput';
import { DiceRoller } from './components/dice/DiceRoller';


function App() {
  return (
  <>
    <div id="app-layout" className='app-layout full-width full-height responsive-flex'>
      <div><h1>Solo TTRPG Toolkit</h1></div>
      <div id='dice-roller' className="default-border max-height dice1-area"><DiceRoller /></div>
      <div className='default-border dice2-area pad-1'><DiceInput /></div>
      <div id='card-drawer'className='default-border card-area overflow-clip pad-1'><CardDraw /></div>
    </div>
  </>
  );
}

export default App
