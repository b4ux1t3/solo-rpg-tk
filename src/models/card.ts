export const Ranks = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'Joker']
export const Suits = ['Hearts', 'Spades', 'Diamonds', 'Clubs']
export type Rank = string | number
export type Suit = string
export type Card = {
    Rank: Rank;
    Suit: Suit;
}

const stripJokers = (deck: Card[], addJokers = false) => {
    const nojokerDeck = deck.filter(c => c.Rank !== 'Joker');

    if (!addJokers) return nojokerDeck;

    return [
        ...nojokerDeck,
        ...deck
            .filter(c => c.Rank === 'Joker')
            .splice(2)
    ];
}

export const generateDeck = (jokers = true): Card[] =>
    stripJokers(
        Ranks.flatMap(
            r =>
                Suits.map(
                    s =>
                        ({ Rank: r, Suit: s } as Card))), jokers
    );