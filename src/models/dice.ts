export const rand = (n: number): number => Math.ceil(Math.random() * n);

export const supportedDice = [2, 3, 4, 6, 8, 10, 12, 20, 100];