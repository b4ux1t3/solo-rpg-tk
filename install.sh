#!/bin/sh
rm -rf /var/www/solo-rpg-tk
cp -r dist /var/www/solo-rpg-tk
chown root: /var/www/solo-rpg-tk
cp solo-rpg-tk.nginx.conf /etc/nginx/sites-available/solo-rpg-tk
ln -s /etc/nginx/sites-available/solo-rpg-tk /etc/nginx/sites-enabled/solo-rpg-tk
