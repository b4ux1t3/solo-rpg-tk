# Solo TTRPG Toolkit
A React-based, single-page application for use with solo TTRPGs. Roll dice, draw cards. That's it.

## Running
This application is running on GitLab. Click the link in the sidebar for GitLab Pages!